#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=ethash.unmineable.com:3333
WALLET=BTT:THatgA4VF3xkJVcNKxHL8fu7sC3G9zRQbj.anmin

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./dua --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    chmod +x ./dua --algo ETHASH --pool $POOL --user $WALLET $@
done
